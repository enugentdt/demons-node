package demonsnet

import (
	"fmt"

	"github.com/golang/protobuf/proto"
	"github.com/golang/protobuf/ptypes/timestamp"
	"gitlab.com/demilletech/DEMONS/node/demons"
)

type Message struct {
	From      string
	To        string
	Content   string
	Timestamp *timestamp.Timestamp
}

func SendMessage(to string, content string, client *Connection) {
	remotePubkey := GetClientPubKey(to)

	header := CreateHeader(GenMessageID())

	messageContent := demons.MessageContent{
		Content: content,
		Header:  header,
		To:      to,
		From:    _clientID}

	unencryptedContent, _ := proto.Marshal(&messageContent)
	wrappedContent := WrapMessage(unencryptedContent, demons.MessageType_M_MessageContent)
	encryptedContent := EncryptMessageWithFrom(wrappedContent, remotePubkey, _clientID)

	message := demons.Message{
		Header:  CreateHeader(GenMessageID()),
		To:      to,
		Content: encryptedContent}

	marshalled_message, _ := proto.Marshal(&message)
	wrapped := WrapMessage(marshalled_message, demons.MessageType_M_Message)
	EncryptAndSendWithDestConn(wrapped, client, client)
}

func GotMessage(dmessage *demons.MessageContent, client *Connection) {
	message := Message{
		From:      dmessage.From,
		To:        dmessage.To,
		Content:   dmessage.Content,
		Timestamp: dmessage.Timestamp}

	fmt.Printf("Got message from %s: %s\n", message.From, message.Content)
}
