package demonsnet

import (
	"bytes"
	"fmt"
	"time"

	"github.com/golang/protobuf/ptypes/timestamp"

	"github.com/golang/protobuf/proto"
	"gitlab.com/demilletech/DEMONS/node/demons"
	"gitlab.com/demilletech/DEMONS/node/demonscrypto"
)

const messageIDLength = 32

func NewMessage(encodedData []byte, client *Connection) {
	mtype, decodedData := GetMessageType(encodedData)
	switch mtype {
	case demons.MessageType_M_SignedData:
		message := demons.SignedData{}
		proto.Unmarshal(decodedData, &message)

		Handle_Signed_Data(&message, client)
	default:
		println("Data was not signed\n")
	}
}

func HandleMessage(encodedData []byte, client *Connection) {
	mtype, decodedData := GetMessageType(encodedData)

	switch mtype {
	case demons.MessageType_M_ACK:
		message := demons.ACK{}
		proto.Unmarshal(decodedData, &message)

		Handle_ACK(&message, client)

	case demons.MessageType_M_SYN_ACK:
		message := demons.SYN_ACK{}
		proto.Unmarshal(decodedData, &message)

		Handle_SYN_ACK(&message, client)

	case demons.MessageType_M_SYN:
		message := demons.SYN{}
		proto.Unmarshal(decodedData, &message)

		Handle_SYN(&message, client)
	case demons.MessageType_M_EncryptedData:
		message := demons.EncryptedData{}
		proto.Unmarshal(decodedData, &message)

		Handle_EncryptedData(&message, client)
	default:
		println("Couldn't decode message\n")
	}

}

func HandleDecrypted(decrypted []byte, client *Connection) {
	mtype, decodedData := GetMessageType(decrypted)

	switch mtype {
	case demons.MessageType_M_Message:
		message := demons.Message{}
		proto.Unmarshal(decodedData, &message)

		Handle_Message(&message, client)
	case demons.MessageType_M_MessageContent:
		message := demons.MessageContent{}
		proto.Unmarshal(decodedData, &message)

		Handle_MessageContent(&message, client)
	case demons.MessageType_M_PubkeyRequest:
		message := demons.PubkeyRequest{}
		proto.Unmarshal(decodedData, &message)

		Handle_PubkeyRequest(&message, client)
	case demons.MessageType_M_PubkeyResponse:
		message := demons.PubkeyResponse{}
		proto.Unmarshal(decodedData, &message)

		Handle_PubkeyResponse(&message, client)
	case demons.MessageType_M_EncryptedData:
		message := demons.EncryptedData{}
		proto.Unmarshal(decodedData, &message)

		Handle_EncryptedData(&message, client)
	default:
		fmt.Printf("Couldn't decrypt data of type %s\n", mtype.String())
	}
}

func GetMessageType(data []byte) (demons.MessageType, []byte) {
	var wrapped demons.MessageContainer

	proto.Unmarshal(data, &wrapped)

	return wrapped.MessageType, wrapped.Data
}

func Handle_Signed_Data(message *demons.SignedData, client *Connection) {
	signature := message.Sig
	data := message.Data

	computedSig := SignData(data, signature.SigAlg)

	if bytes.Compare(computedSig, signature.Sig) != 0 {
		println("Signature does not match")
		return
	}

	HandleMessage(data, client)

}

func Handle_EncryptedData(message *demons.EncryptedData, client *Connection) {
	//header := message.Header
	data := message.Data

	//fmt.Printf("Got EncryptedData from %s\n", header.ClientId)

	pubkey := GetClientPubKey(message.From)

	decrypted := demonscrypto.DecryptData(data, pubkey)
	HandleDecrypted(decrypted, client)
}

func Handle_Message(message *demons.Message, client *Connection) {
	to := GetClient(message.To)
	EncryptAndSendWithDestConn(message.Content, to, to)
}

func Handle_MessageContent(message *demons.MessageContent, client *Connection) {
	GotMessage(message, client)
}

func Handle_PubkeyRequest(message *demons.PubkeyRequest, client *Connection) {
	pubkey := GetClientPubKey(message.ClientId)
	response := demons.PubkeyResponse{
		Header:   CreateHeader(GenMessageID()),
		ClientId: message.ClientId,
		Pubkey:   pubkey}

	marshalled_response, _ := proto.Marshal(&response)
	wrapped := WrapMessage(marshalled_response, demons.MessageType_M_PubkeyResponse)
	EncryptAndSendWithDestConn(wrapped, client, client)
	fmt.Printf("Got request for %s\n", message.ClientId)
}

func Handle_PubkeyResponse(message *demons.PubkeyResponse, client *Connection) {
	pubkey := message.Pubkey
	clientIDForPubkey := message.ClientId
	//fmt.Printf("Setting pubkey for %s\n%v\n", clientIDForPubkey, pubkey)
	SetClientPubkey(clientIDForPubkey, pubkey)
}

func Handle_SYN(message *demons.SYN, client *Connection) {
	//fmt.Printf("Got SYN from %s\n", message.Header.ClientId)
	client.PubKey = message.Pubkey
	client.ClientID = message.Header.ClientId

	SetClient(client.ClientID, client)

	header := CreateHeader(GenMessageID())

	syn_ack := demons.SYN_ACK{
		ClientType: demons.ClientType_NODE,
		Header:     header,
		OtherNodes: []string{},
		Pubkey:     demonscrypto.GetPubKey()}

	marshalled_synack, _ := proto.Marshal(&syn_ack)
	wrapped := WrapMessage(marshalled_synack, demons.MessageType_M_SYN_ACK)
	SignAndSend(wrapped, client)
}

func Handle_SYN_ACK(message *demons.SYN_ACK, client *Connection) {
	//fmt.Printf("Got SYN_ACK from %s\n", message.Header.ClientId)
	client.PubKey = message.Pubkey
	client.ClientID = message.Header.ClientId

	SetClient(client.ClientID, client)

	header := CreateHeader(GenMessageID())
	ack := demons.ACK{
		Header:     header,
		OtherNodes: []string{}}

	marshalled_ack, _ := proto.Marshal(&ack)
	wrapped := WrapMessage(marshalled_ack, demons.MessageType_M_ACK)

	SignAndSend(wrapped, client)
}

func Handle_ACK(message *demons.ACK, client *Connection) {
	//fmt.Printf("Got ACK from %s\n", message.Header.ClientId)
	fmt.Printf("New connection: %s\n", message.Header.ClientId)
	SetClient(client.ClientID, client)
}

func WrapMessage(message []byte, messageType demons.MessageType) []byte {
	wrapped := demons.MessageContainer{
		Data:        message,
		MessageType: messageType}
	marshalled, _ := proto.Marshal(&wrapped)
	return marshalled
}

func CreateHeader(messageID string) *demons.Header {
	return CreateHeaderWithClientID(messageID, _clientID)
}

func CreateHeaderWithClientID(messageID string, clientID string) *demons.Header {
	return &demons.Header{
		ClientId:  clientID,
		MessageId: messageID,
		Timestamp: &timestamp.Timestamp{Seconds: int64(time.Now().Unix())}}
}

func GenMessageID() string {
	return demonscrypto.GenChars(messageIDLength)
}
