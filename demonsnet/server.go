package demonsnet

import (
	"fmt"
	"log"
	"net"
	"time"

	"github.com/golang/protobuf/proto"

	"gitlab.com/demilletech/DEMONS/node/demons"
)

var ln *net.Listener

var conns map[string]*Connection

var keys map[string]*[]byte

var newConns []*Connection

type Connection struct {
	Conn     net.Conn
	ClientID string
	PubKey   []byte
	Closed   bool
}

var _clientID string

type ClientType int

const (
	Node   ClientType = 0
	Client ClientType = 1
)

var _clientType demons.ClientType

func Init(clientType ClientType, clientID string) {
	_clientID = clientID
	SetSelfClientType(clientType)
}

func SetSelfClientType(clientType ClientType) {

	switch clientType {
	case Node:
		_clientType = demons.ClientType_NODE
	case Client:
		_clientType = demons.ClientType_CLIENT
	}
}

func StartServer() error {
	conns = make(map[string]*Connection)
	keys = make(map[string]*[]byte)
	newConns = make([]*Connection, 1024)

	err := Listen()
	return err
}

func GetClientPubKeyNoPoll(clientID string) []byte {
	if val, ok := keys[clientID]; ok {
		return *val
	}
	return nil
}

func GetClientPubKey(clientID string) []byte {
	key := GetClientPubKeyNoPoll(clientID)
	if len(key) == 0 {
		if _clientType == demons.ClientType_CLIENT {
			RequestPubkey(clientID, _client)
			for len(GetClientPubKeyNoPoll(clientID)) == 0 {
				time.Sleep(time.Millisecond * 1)
			}
			key = GetClientPubKeyNoPoll(clientID)
		}
	}
	return key
}

func GetClient(clientID string) *Connection {
	f := conns[clientID]
	if f == nil {
		return nil
	}
	return f
}

func SetClient(clientID string, client *Connection) {
	if clientID == "" {
		fmt.Printf("ClientID is empty\n")
		return
	}
	conns[clientID] = client
	if client.PubKey != nil {
		SetClientPubkey(clientID, client.PubKey)
	} else {
		fmt.Printf("Client pubkey is nil: %s\n", clientID)
	}
}

func SetClientPubkey(clientID string, pubkey []byte) {
	keys[clientID] = &pubkey
}

func RequestPubkey(clientID string, conn *Connection) {
	request := demons.PubkeyRequest{
		ClientId: clientID,
		Header:   CreateHeader(GenMessageID())}

	marshalled_request, _ := proto.Marshal(&request)

	wrapped := WrapMessage(marshalled_request, demons.MessageType_M_PubkeyRequest)

	EncryptAndSendWithDestConn(wrapped, conn, conn)
}

func handleConnection(client *Connection) {
	buf := make([]byte, 1024)
	for {
		n, err := client.Conn.Read(buf)
		if err != nil {
			if err.Error() == "EOF" {
				fmt.Printf("Client closed\n")
				client.Closed = true
				return
			}
			log.Fatal(err)
		}

		go NewMessage(buf[:n], client)
	}
}

func Listen() error {
	ln, err := net.Listen("tcp", ":1337")
	if err != nil {
		return err
	}
	for {
		conn, err := ln.Accept()
		if err != nil {
			// handle error
			log.Fatal(err)
		}

		newc := CreateConnection(conn)
		newConns = append(newConns, newc)
		go handleConnection(newc)
	}
}
