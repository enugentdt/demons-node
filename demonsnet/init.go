package demonsnet

import (
	"net"
	"time"

	"github.com/golang/protobuf/proto"
	"gitlab.com/demilletech/DEMONS/node/demons"
	"gitlab.com/demilletech/DEMONS/node/demonscrypto"
)

var _client *Connection

func ConnectTo(address string) *Connection {
	header := CreateHeader(GenMessageID())

	message := demons.SYN{
		ClientType: demons.ClientType_CLIENT,
		Header:     header,
		Pubkey:     demonscrypto.GetPubKey()}

	marshalled_syn, _ := proto.Marshal(&message)
	wrapped := WrapMessage(marshalled_syn, demons.MessageType_M_SYN)

	netconn, _ := net.Dial("tcp", address)

	conn := CreateConnection(netconn)

	SignAndSend(wrapped, conn)

	go handleConnection(conn)

	for conn.PubKey == nil {
		time.Sleep(time.Millisecond * 1)
	}

	_client = conn

	return _client
}

func CreateConnection(conn net.Conn) *Connection {
	return &Connection{Conn: conn, Closed: false}
}
