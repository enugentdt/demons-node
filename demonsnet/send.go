package demonsnet

import (
	"crypto/sha256"
	"errors"
	"fmt"

	"github.com/golang/protobuf/proto"

	"gitlab.com/demilletech/DEMONS/node/demons"
	"gitlab.com/demilletech/DEMONS/node/demonscrypto"
)

func EncryptMessage(data []byte, pubkey []byte) []byte {

	return EncryptMessageWithFrom(data, pubkey, _clientID)
}

func EncryptMessageWithFrom(data []byte, pubkey []byte, from string) []byte {
	encrypted := demonscrypto.EncryptData(data, pubkey)

	encryptedMessage := demons.EncryptedData{
		Data:   encrypted,
		Header: CreateHeader(GenMessageID()),
		From:   from}

	marshalled_encrypted, _ := proto.Marshal(&encryptedMessage)

	wrapped := WrapMessage(marshalled_encrypted, demons.MessageType_M_EncryptedData)
	return wrapped
}

// EncryptAndSend will also sign data and then send it
func EncryptAndSend(data []byte, client *Connection, dest []byte) {
	wrapped := EncryptMessage(data, dest)
	SignAndSend(wrapped, client)
}

func EncryptAndSendWithDestConn(data []byte, client *Connection, dest *Connection) {
	EncryptAndSend(data, client, dest.PubKey)
}

func EncryptAndSendWithDestID(data []byte, client *Connection, dest string) {
	pubkey := GetClientPubKey(dest)

	if pubkey == nil {
		return
	}

	EncryptAndSend(data, client, pubkey)
}

func SignData(data []byte, hashAlg string) []byte {

	switch hashAlg {
	case "SHA256":
		hashSlingingSlasher := sha256.New()
		hashSlingingSlasher.Write(data)
		finalSignature := hashSlingingSlasher.Sum(nil)
		return finalSignature
	}

	return nil
}

func SignAndSend(data []byte, client *Connection) {
	dataSignature := SignData(data, "SHA256")

	signature := demons.Signature{
		PubkeyId: []byte("despacito"),
		Sig:      dataSignature,
		SigAlg:   "SHA256"}
	signed := demons.SignedData{
		Data: data,
		Sig:  &signature}

	signedData, _ := proto.Marshal(&signed)

	wrapped := demons.MessageContainer{
		Data:        signedData,
		MessageType: demons.MessageType_M_SignedData}

	wrappedData, _ := proto.Marshal(&wrapped)

	WriteData(wrappedData, client)
}

func WriteData(data []byte, client *Connection) error {
	if !client.Closed {
		client.Conn.Write(data)
	} else {
		fmt.Printf("Connection is closed\n")
		return errors.New("Connection is closed")
	}
	return nil
}
