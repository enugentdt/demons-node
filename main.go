package main

import (
	"bufio"
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"os"
	"strings"
	"time"

	"gitlab.com/demilletech/DEMONS/node/demonscrypto"

	"gitlab.com/demilletech/DEMONS/node/demonsnet"
)

var clientID string

var ctype demonsnet.ClientType = demonsnet.Node

func main() {
	clientID = GenClientID()
	println(clientID)

	demonscrypto.GenerateKey()

	/*indata := []byte("Hello, World!")

	fmt.Printf("%s\n", string(indata))

	encrypted := demonscrypto.EncryptData(indata)

	dest := make([]byte, base64.StdEncoding.EncodedLen(len(encrypted)))
	base64.StdEncoding.Encode(dest, encrypted)

	fmt.Printf("%s\n", dest)

	decrypted := demonscrypto.DecryptData(encrypted)

	fmt.Printf("%s\n", string(decrypted))

	return*/

	demonsnet.Init(ctype, clientID)

	if ctype == demonsnet.Node {

		err := demonsnet.StartServer()
		if err != nil {
			ctype = demonsnet.Client
			//ui.StartUI()

			demonsnet.SetSelfClientType(ctype)
			NewClient(demonsnet.ConnectTo("127.0.0.1:1337"))

			return
		}

		for {
			time.Sleep(time.Second * 1)
		}
	}

}

func NewClient(client *demonsnet.Connection) {
	fmt.Printf("Who are you sending to?\n> ")
	reader := bufio.NewReader(os.Stdin)
	text, _ := reader.ReadString('\n')
	text = text[:strings.Index(text, "\n")]

	to := text

	for {
		text, _ := reader.ReadString('\n')

		demonsnet.SendMessage(to, text[:strings.Index(text, "\n")], client)
	}
}

func GetClientID() string {
	if clientID == "" {
		clientID = GenClientID()
	}

	return clientID
}

func GenClientID() string {
	randData := make([]byte, 24)

	rand.Read(randData)

	encodedData := make([]byte, base64.StdEncoding.EncodedLen(len(randData)))

	base64.StdEncoding.Encode(encodedData, randData)

	return string(encodedData)
}
