# Welcome to DEMONS

What the hell is demons, you might ask?

It's a fully encrypted client that can send data back and forth between two autodiscovering peers. It's designed as a chat client, however, the code can be refactored to do almost anything

## Building

Download a repository

If you want to run a dev version, just run `go run main.go`

If you want to build, `go build`

It's pretty self explanatory.

## Cryptographic information

Keys are generated on each run of the server, and are tied to a unique identifier for the client. That identifier is ephemeral and is lost when the client is killed.

The repository uses NaCl from Go for encryption and decryption, running ed25519 keys. The public keys are transmitted over the wire, and all messages are signed before transmission, along with being verified on the other side.

## Issues

See something wrong? Open an issue on the repository's GitLab

## Contributing

Want to help out? Fork the repository, make your change, and create a pull request. Include a description of the change made and its purpose.
