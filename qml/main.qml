import QtQuick 2.9
import QtQuick.Controls 2.2

ApplicationWindow {
    id: applicationWindow
    visible: true
    width: 640
    height: 480
    property alias applicationWindow: applicationWindow
    title: qsTr("Scroll")

    ScrollView {
        anchors.fill: parent

        ListView {
            id: listView
            width: 640
            height: 480
            model: 20
            delegate: ItemDelegate {
                text: "Item " + (index + 1)
                width: parent.width
            }

            TextArea {
                id: textArea
                height: 378
                text: qsTr("Text goes here")
                wrapMode: Text.WordWrap
                anchors.right: parent.right
                anchors.rightMargin: 8
                anchors.left: parent.left
                anchors.leftMargin: 8
                anchors.top: parent.top
                anchors.topMargin: 8
                placeholderText: "Chat messages go here..."
            }

            TextField {
                id: textField
                y: 383
                width: 465
                height: 83
                text: qsTr("Text Field")
                placeholderText: "Send a message"
                anchors.left: parent.left
                anchors.leftMargin: 25
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 8
            }

            Button {
                id: button
                x: 537
                y: 407
                text: qsTr("Send")
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 30
                anchors.right: parent.right
                anchors.rightMargin: 50
            }
        }
    }
}

/*##^## Designer {
    D{i:8;anchors_width:640}D{i:9;anchors_x:25}D{i:2;anchors_height:480;anchors_width:640}
}
 ##^##*/
