package demonscrypto

import (
	"crypto/rand"
	"fmt"
	"log"
	mathrand "math/rand"

	"golang.org/x/crypto/nacl/box"
)

var pri *[32]byte
var pub *[32]byte
var tpub []byte
var tempNonce = [24]byte{218, 195, 252, 156, 79, 30, 11, 214, 9, 180, 93, 196, 184, 50, 153, 50, 48, 191, 0, 68, 33, 156, 20, 203}

const lower = "abcdefghijklmnopqrstuvwxyz"
const upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
const nums = "0123456789"
const characters = lower + upper + nums

func GenerateKey() {
	var err error
	pub, pri, err = box.GenerateKey(rand.Reader)
	if err != nil {
		log.Fatal(err)
	}
}

func GenNonce() *[24]byte {
	var t = make([]byte, 24)
	var out [24]byte
	rand.Read(t)
	copy(out[:], t[:])
	return &out
}

func GenChars(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = characters[mathrand.Int63()%int64(len(characters))]
	}
	return string(b)
}

func StringToNonce(data string) *[24]byte {
	var out [24]byte
	temp := []byte(data)
	copy(out[:], temp[:])
	return &out
}

func GetPubKey() []byte {
	if tpub == nil {
		tpub = make([]byte, len(pub))
		copy(tpub, pub[:])
	}
	return tpub
}

func EncryptData(data []byte, pubkey []byte) []byte {
	var tpub [32]byte
	copy(tpub[:], pubkey[:])
	return box.Seal(nil, data, StringToNonce("Hello World!"), &tpub, pri)
}

func DecryptData(data []byte, pubkey []byte) []byte {
	var tpub [32]byte
	copy(tpub[:], pubkey[:])
	d, _ := box.Open(nil, data, StringToNonce("Hello World!"), &tpub, pri)
	return d
}

func printKey() {

	fmt.Printf("Private: %v\n", pri)
	fmt.Printf("Public: %v\n", pub)
	println(len(pri))
	println(len(pub))
}
